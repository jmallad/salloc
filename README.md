salloc is a Small, Slow, Simple dynamic memory allocator, intended for use
on resource-constrained microcontrollers. There are two ways to use salloc:

1. Include src/salloc.c and include/private/salloc.h in your project.
This is the easiest method, and the only one that works for a micro
controller.

2. Build salloc as a shared library and include libsalloc.so and
include/public/salloc.h in your project. This method is used for most
executables in modern operating systems.

There are a few configurable values in include/private/salloc.h. They are
well-commented. You must recompile the code after changing the values.
