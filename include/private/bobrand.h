#pragma once

#include "salloc.h"

struct bobctx {
	uint32_t a;
	uint32_t b;
	uint32_t c;
	uint32_t d;
};

uint32_t bobrand(struct bobctx* x);
