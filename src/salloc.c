#include "salloc.h"

/* Actual heap that is pointed to by return addresses from salloc */
uint8_t salloc_heap[AVAILABLE_BYTES+1] = {0};
/* Pool of blocks which may be allocated */
struct salloc_block salloc_blockpool[SALLOC_N_BLOCKS] = {0};
/* List of allocated blocks */
struct salloc_block* salloc_blocks = NULL;
/* Indicates the number of free blocks */
unsigned n_free = 1;
/* Denotes the size of the largest free slice in bytes */
salloc_addr largest_free = AVAILABLE_BYTES;
#if (SALLOC_USABILITY)
/* This abstract value represents the result of a calculation which intends to
   determine the "usability" of a particular block pool. Highly fragmented 
   block pools with many small free slices have a low usability score. */
unsigned usability = 100;
#if (SALLOC_REBALANCE)
/* This value determines when the object rebalancer triggers. If the usability
   score reaches this value or lower, objects in the page are moved and 
   re-assigned to create more usable free space. */
unsigned threshold = 15;
#endif
#endif
/****** Static functions ******/

#if (SALLOC_USABILITY)
static salloc_addr sum_free_blocks(struct salloc_block* blocks,
				   unsigned n_blocks)
{
	unsigned i;
	salloc_addr ret = 0;
	for (i = 0; i < n_blocks; i++) {
		if (!blocks[i].used) {
			ret += blocks[i].size;
		}
	}
	return ret;
}

static salloc_addr average_free_blocks(struct salloc_block* blocks,
				       unsigned n_blocks, unsigned n_free)
{
	salloc_addr ret;
	if (!n_free) {
		return 0;
	}
	ret = sum_free_blocks(blocks, n_blocks);	
	ret /= n_free;
	return ret;
}

static float scale(salloc_addr x, salloc_addr oldmax, salloc_addr newmax)
{
	return (float)newmax * ((float)x / (float)oldmax);
}


#if (!SALLOC_MATH)
#include <math.h>
#endif
static unsigned score(struct salloc_block* blocks,
		      unsigned n_blocks,
		      unsigned n_free,
		      salloc_addr pagesize,
		      unsigned granularity)
{
	unsigned ret;
#if (SALLOC_MATH)
	unsigned exp;
	unsigned i;
#endif
	ret = average_free_blocks(blocks, n_blocks, n_free);
	ret = (unsigned)scale(ret, pagesize, granularity);
#if (SALLOC_MATH)
	exp = pagesize / sum_free_blocks(blocks, n_blocks);
	for (i = 0; i < exp-1; i++) {
		ret *= ret;
	}
#else
	ret = (unsigned)pow(ret,
			    (pagesize / sum_free_blocks(blocks, n_blocks)));
#endif
	return ret;
}

static void usability_score()
{
#if (SALLOC_REBALANCE)
	unsigned ret;
#endif
	usability = score(salloc_blockpool, SALLOC_N_BLOCKS, n_free,
			  AVAILABLE_BYTES, 100);
#if (SALLOC_REBALANCE)
	if (usability <= threshold) {
		do {
			ret = rebalance_objects(salloc_blockpool,
						SALLOC_N_BLOCKS,
						salloc_addr pagesize);
			usability = score(salloc_blockpool, SALLOC_N_BLOCKS,
					  n_free, AVAILABLE_BYTES, 100);
		}
		while (ret && usability <= threshold);
	}
#endif
}
#if (SALLOC_REBALANCE)
static void rebalance_object(struct salloc_block* pool, unsigned n_blocks,
			     unsigned di, unsigned si)
{
	
}

static unsigned rebalance_objects(struct salloc_block* pool, unsigned n_blocks,
			      salloc_addr pagesize)
{
	unsigned i;
	unsigned f; /* Index of first empty block */
	unsigned s; /* Size of block pointed to by f */
	unsigned ret = 0;
	/* First, move the pointers */
	for (i = 0; i < n_blocks; i++) {
		if (!f &&
		    !pool[i].used &&
		    pool[i].size >= sizeof(salloc_addr)) {
			f = i;
		}
		/* If bit 2 is set in the used field, this object is a pointer
		   to a dynamic object. Thus, this object may be rebalanced. */
		if (pool[i].used >> 1 && f) {
			rebalance_object(pool, n_blocks, f, i);
			f = 0;
		}
	}
}
#endif
#endif

static struct salloc_block* alloc_block(struct salloc_block* pool,
				 unsigned n_blocks)
{
	unsigned i;
	for (i = 0; i < n_blocks; (i) ++) {
		if (!pool[i].used && !pool[i].size) {
			return &pool[i];
		}
	}
	return NULL;
}

static void free_block(struct salloc_block* p)
{
	if (p->prev) {
		p->prev->next = p->next;
	}
	if (p->next) {
		p->next->prev = p->prev;
	}
	memset(p, 0, sizeof(struct salloc_block));
}

static void* fragment_block(struct salloc_block* p, unsigned size)
{
	struct salloc_block* f;
	if (!p) {
		return NULL;
	}
	p->used = 1;
	if (p->size == size) {
		n_free--;
		usability_score();
		return &salloc_heap[p->start];
	}
	f = alloc_block(salloc_blockpool, SALLOC_N_BLOCKS);
	if (!f) {
		return NULL;
	}
	if (f->prev) {
		f->prev->next = f->next;
	}
	if (f->next) {
		f->next->prev = f->prev;
	}
	f->size = p->size - size;
	f->start = p->start + size;
	p->size = size;
	if (p->next) {
		f->next = p->next;
		p->next->prev = f;
	}
	p->next = f;
	f->prev = p;
	if (f->size > largest_free) {
		largest_free = f->size;
	}
	usability_score();
	return &salloc_heap[p->start];
}

/****** Global functions ******/

#if (SALLOC_MEMSET == 1)
/* Fill the first size bytes of the memory area pointed to by out with the 
   constant byte c */
void* memset(void* out, int c, size_t size)
{
	size_t i;
	uint8_t* p;
	p = out;
	for (i = 0; i < size; i++) {
		p[i] = c;
	}
	return out;
}
#endif

/* Attempt to allocate size bytes from the salloc heap. Returns a pointer to
   the start of the newly-allocated area if successful. */
void* salloc(size_t size)
{
	size_t i = 0;
	struct salloc_block* ret = NULL;
	struct salloc_block* p = salloc_blocks;
	/* Round up size to the nearest multiple of 8 */
#if (SALLOC_ROUND)
	while (size % SALLOC_ROUND) {
		size++;
	}
#endif
	if (size > AVAILABLE_BYTES) {
		/* Requested size is too large */
		return NULL;
	}
	if (largest_free && size > largest_free) {
		return NULL;
	}
	if (!p) {
		/* Table is empty, insert the first block */
		salloc_blocks = salloc_blockpool;
		p = salloc_blocks;		
		p->size = size;
		p->used = 1;
		p->prev = NULL;
		p->next = &p[1];
		p[1].start = size+1;
		p[1].size = AVAILABLE_BYTES - size;
		p[1].prev = p;
		largest_free = p[1].size;
		return salloc_heap;
	}
	while (p && i < SALLOC_N_BLOCKS - 1) {
		if (!p->used && p->size >= size) {
			if (!ret) {
				ret = p;
				continue;
			}
			if (p->size < ret->size) {
				ret = p;
			}
		}
		i++;
		p = p->next;
	}
	return fragment_block(ret, size);
}

/* Free an area previously returned by salloc. The area is zeroed and the block
   is released. */
void sfree(void* p)
{
	unsigned i;
	struct salloc_block* b;
	if (!p) {		
		return;
	}
	b = salloc_blocks;
	for (i = 0; i < SALLOC_N_BLOCKS; i++) {
		if (!b) {
			return;
		}
		if (b->start > SALLOC_MAX_ADDR) {
			return;
		}
		if (&salloc_heap[b->start] == p) {
			break;
		}
		if (!b->next) {
			return;			
		}
		b = b->next;
	}
	if (b->next) {
		if (!b->next->used && b->next->size) {
			b->size += b->next->size;
			free_block(b->next);
			n_free--;
		}
	}
	if (b->prev) {
		if (!b->prev->used && b->prev->size) {
			b = b->prev;
			b->size += b->next->size;
			free_block(b->next);
			n_free--;
		}
	}
	b->used = 0;
	if (b->size > largest_free) {
		largest_free = b->size;
	}
#if (SALLOC_ZEROFREE)
	memset(&salloc_heap[b->start], 0, b->size);
#endif
	n_free++;
	usability_score();
}
