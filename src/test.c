#include <stdio.h>
#include "salloc.h"
#include "bobrand.h"

/* These externs aren't needed for regular applications. They are used in the
   dump_blocks() implementation below. */
extern uint8_t salloc_heap[AVAILABLE_BYTES+1];
extern struct salloc_block* salloc_blocks;
extern struct salloc_block salloc_blockpool[SALLOC_N_BLOCKS];
extern unsigned usability;
/*  
    This 16-byte context represents the internal state of the pseudo-random
number generator. Here is an easy way to generate valid values on most Linux
systems:

for num in $(od -An -tx4 -N16 /dev/urandom); do echo -n "0x$num, "; done; echo 
*/
struct bobctx salloc_ctx = {0x7bc3abdf, 0x1fa3617a, 0x5bf07b60, 0x1a9c09d0};

/* Dump the contents of the block table in human-readable format to stdout. */
static void dump_blocks()
{
	unsigned i;
	struct salloc_block* p = salloc_blocks;
	printf("------ ALLOCATED BLOCKS LIST ------\n");
	printf("Usability score: %u\n", usability);
	for (i = 0; i < SALLOC_N_BLOCKS; i++) {
		if (p->size) {
			if (p->used) {
				printf("[Block %04u] @ %p:%08x size %u\n",
				       i, &salloc_heap[p->start],
				       p->start, p->size);
			}
			else {
				printf("[Empty %04u] @ %p:%08x size %u\n",
				       i, &salloc_heap[p->start],
				       p->start, p->size);
			}
		}
		if (!p->next) {
			break;
		}
		p = p->next;
	}
	printf("------ ------ ------\n");
}

int main()
{
	uint8_t* a;
	uint8_t* b;
	uint8_t* c;
	uint8_t* d;
	uint8_t i;
	uint8_t p;
	uint8_t size;
	uint8_t* obj[20] = {0};
	printf("Size of single block:       %lu\n",
	       sizeof(struct salloc_block));
	printf("Size of block pool:         %lu\n",
	       sizeof(struct salloc_block) * SALLOC_N_BLOCKS);
	printf("Total address space:        %lu\n", SALLOC_MAX_ADDR);
	printf("Available allocation space: %lu\n", AVAILABLE_BYTES);
	printf("Heap start  : %p\n", salloc_heap);
	printf("###### Begin simple linear allocation test ######\n");
	a = malloc(12);
	dump_blocks();
	b = malloc(2);
	dump_blocks();
	c = malloc(20);
	dump_blocks();
	d = malloc(42);
	dump_blocks();

	printf("Allocated at: %p,  %p,  %p,  %p\n", a, b, c, d);
	printf("Differences : %li, %li, %li, %li\n",
	       a - salloc_heap, b - salloc_heap, c - salloc_heap,
	       d - salloc_heap);
	
	free(b);
	dump_blocks();
	free(c);
	dump_blocks();
	free(d);
	dump_blocks();
	free(a);
	dump_blocks();
	printf("###### End simple linear allocation test ######\n");
	printf("An empty block table indicates a successful result.\n");
	printf("###### Begin randomized allocation test ######\n");
	memset(obj, 0, sizeof(uint8_t*) * 10);
	for (i = 0; i < 40; i++) {
		p = bobrand(&salloc_ctx) % 20;
		if (obj[p]) {
			printf("Free at %p\n", obj[p]);
			free(obj[p]);
			obj[p] = NULL;
		}
		else {
			size = bobrand(&salloc_ctx) % 100;
			obj[p] = malloc(size);
			printf("Alloc %u at %p\n", size, obj[p]);
		}
		dump_blocks();					
	}
	for (i = 0; i < 20; i++) {
		if (obj[i]) {
			printf("Free at %p\n", obj[i]);
			free(obj[i]);
			dump_blocks();
		}
	}
	return 0;
}

